{
  description = "Self Nix Overlay";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/master";
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = args@{ self, flake-utils, nixpkgs, ... }:
    {
      overlay = final: prev: {
        inherit (self.packages.${final.system})
          yabai-bin qutebrowser-bin fcitx5-pinyin-zhwiki style-detector
          lxgw-wenkai-lite-font sketchybar librewolf-bin;
      };
    } // flake-utils.lib.eachSystem [ "aarch64-darwin" "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          allowBroken = true;
          allowUnsupportedSystem = true;
        };
      in {
        packages = rec {
          yabai-bin = pkgs.callPackage ./pkgs/yabai-bin { };
          qutebrowser-bin = pkgs.callPackage ./pkgs/qutebrowser-bin { };
          fcitx5-pinyin-zhwiki =
            pkgs.callPackage ./pkgs/fcitx5-pinyin-zhwiki { };
          style-detector = pkgs.callPackage ./pkgs/style-detector { };
          lxgw-wenkai-lite-font =
            pkgs.callPackage ./pkgs/lxgw-wenkai-lite-font { };
          sketchybar = pkgs.callPackage ./pkgs/sketchybar { };
          librewolf-bin = pkgs.callPackage ./pkgs/librewolf-bin { };
        };
      });
}
