{ lib, fetchurl, stdenv }:
let
  version = "0.2.3";
  subversion = "20211121";
in stdenv.mkDerivation rec {
  name = "fcitx5-pinyin-zhwiki";

  src = fetchurl {
    url =
      "https://github.com/felixonmars/fcitx5-pinyin-zhwiki/releases/download/${version}/zhwiki-${subversion}.dict.yaml";
    sha256 = "sha256-/tI9MteF78oCsVBCy9kYKAfD4nnHLdwTkHpUC6g5fhU=";
  };

  #phases = [ "installPhase" ]; # bypass unpack phase.
  unpackPhase = ":"; # maybe batter ?

  installPhase = ''
    mkdir -p $out
    cp -r $src $out/zhwiki.dict.yaml
  '';

  meta = with lib; {
    description = "Fcitx 5 Pinyin Dictionary from zh.wikipedia.org";
    homepage = "https://github.com/felixonmars/fcitx5-pinyin-zhwiki";
    platforms = platforms.all;
    maintainers = with maintainers; [ self ];
  };
}
