{ lib, fetchurl, stdenv, undmg }:
let version = "2.4.0";
in stdenv.mkDerivation rec {
  name = "qutebrowser-bin";
  src = fetchurl {
    url =
      "https://github.com/qutebrowser/qutebrowser/releases/download/v${version}/qutebrowser-${version}.dmg";
    sha256 = "sha256-bJQfn1h0K3DpaJMAumY7d5f8y/Iclr3jzECOK2wEg3o=";
  };

  nativeBuildInputs = [ undmg ];

  sourceRoot = ".";
  
  installPhase = ''
    mkdir -p $out/Applications/
    /usr/bin/xattr -rd com.apple.quarantine qutebrowser.app
    cp -a qutebrowser.app $out/Applications/
  '';

  meta = with lib; {
    description = "Qutebrowser.app";
    homepage = "https://github.com/qutebrowser/qutebrowser";
    license = licenses.gpl3Plus;
    platforms = platforms.all;
    maintainers = with maintainers; [ self ];
  };
}
