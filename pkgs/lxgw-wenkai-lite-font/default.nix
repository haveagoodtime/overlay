{ lib, fetchurl, stdenv }:
let version = "1.111";
in stdenv.mkDerivation rec {
  name = "lxgw-wenkai-lite-font";
  
  # TODO:
  # Not Finally!
  # Need Remove Duplicate .ttf files:
  # 8.49 MB 94kgb8phjn81br65fzjs1r8sq4p19ij4-LXGWWenKaiLite-Bold.ttf
  # 8.50 MB g6ws1zbl1yscfjhiwivmg3mvqcxg0qy8-LXGWWenKaiMonoLite-Bold.ttf
  # 8.50 MB k2rinwp0p09y5i6da8rfgfwlsjxi2ilp-LXGWWenKaiLite-Regular.ttf
  # 8.51 MB y49ivmbhwrlrp3s6y4kzpp494yfs1ibq-LXGWWenKaiMonoLite-Regular.ttf
  #10.54 MB q6lia234sx1c62spcwd7ba3lamh9g56h-LXGWWenKaiLite-Light.ttf
  #10.54 MB wnxr7rl3m77pz2n3k8ysff181dcamfsv-LXGWWenKaiMonoLite-Light.ttf
  # ⚠️
  
  bold = fetchurl {
    url =
      "https://github.com/lxgw/LxgwWenKai-Lite/releases/download/v${version}/LXGWWenKaiLite-Bold.ttf";
    sha256 = "sha256-IGUkUc9OycVkgdO2hdA99Q/Wz94F8NI1+RMSf6ho/sQ=";
  };
  
  light = fetchurl {
    url =
      "https://github.com/lxgw/LxgwWenKai-Lite/releases/download/v${version}/LXGWWenKaiLite-Light.ttf";
    sha256 = "sha256-9pfqYcSzYZhFYKK8KJV5lQnHLm91RBD2UamPEwNeHeo=";
  };
  
  regular = fetchurl {
    url =
      "https://github.com/lxgw/LxgwWenKai-Lite/releases/download/v${version}/LXGWWenKaiLite-Regular.ttf";
    sha256 = "sha256-p5FkvXlVrOc5CNlNiREusR4v4LEmD90sghVPGyAv7OM=";
  };
  
  boldm = fetchurl {
    url =
      "https://github.com/lxgw/LxgwWenKai-Lite/releases/download/v${version}/LXGWWenKaiMonoLite-Bold.ttf";
    sha256 = "sha256-8lKxCG+XAdaHhCqaxehfHzrkLDQE2FaTMxhdM9w8Ns4=";
  };
  
  lightm = fetchurl {
    url =
      "https://github.com/lxgw/LxgwWenKai-Lite/releases/download/v${version}/LXGWWenKaiMonoLite-Light.ttf";
    sha256 = "sha256-FqSKz/DaMzEP68lw8ziN6Fmdwy63Vr9B+49egwGW0Ic=";
  };

  regularm = fetchurl {
    url =
      "https://github.com/lxgw/LxgwWenKai-Lite/releases/download/v${version}/LXGWWenKaiMonoLite-Regular.ttf";
    sha256 = "sha256-PBFVcubJ2PCEOJ5PcyS6aYYjQM387UqMZtR2A04XrVo=";
  };

  dontUnpack = true;

  sourceRoot = name;

  installPhase = ''
    mkdir -p $out/share/fonts/truetype/lxgw-wenkai
    install -m 664 -D ${bold} ${light} ${regular} ${lightm} ${regularm} ${boldm} "$out/share/fonts/truetype/lxgw-wenkai"
  '';

  meta = with lib; {
    description =
      "An open-source Chinese font derived from Fontworks' Klee One";
    homepage = "https://github.com/lxgw/LxgwWenKai-Lite";
    license = licenses.ofl;
    platforms = platforms.all;
    maintainers = with maintainers; [ self ];
  };
}
