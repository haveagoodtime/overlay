{ pkgs, lib, stdenv, fetchFromGitHub }:

let
  target = {
    "aarch64-darwin" = "arm";
    "x86_64-darwin" = "x86";
  }.${stdenv.hostPlatform.system};

in stdenv.mkDerivation rec {
  pname = "sketchybar";
  version = "2.4.1";

  src = fetchFromGitHub {
    owner = "FelixKratz";
    repo = "SketchyBar";
    rev = "v${version}";
    sha256 = "sha256-u7xdXzw4WngrDq3titt+XW+N53kM3mve4ctm/a0FHyQ=";
  };

  buildInputs = with pkgs.darwin.apple_sdk.frameworks; [
    Carbon
    Cocoa
    SkyLight
  ];

  buildPhase = ''
    make ${target}
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp ./bin/sketchybar_${target} $out/bin/sketchybar
  '';

  meta = with lib; {
    description =
      "A custom macOS statusbar with shell plugin, interaction and graph support";
    homepage = "https://github.com/FelixKratz/SketchyBar";
    platforms = platforms.all;
    license = licenses.gpl3;
    maintainers = with maintainers; [ self ];
  };
}
