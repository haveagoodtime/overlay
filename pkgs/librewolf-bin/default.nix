{ lib, fetchurl, stdenv, undmg }:
let version = "95.0-1_aarch64_exp";
in stdenv.mkDerivation rec {
  name = "librewolf-bin";
  src = fetchurl {
    url =
      "https://gitlab.com/librewolf-community/browser/macos/uploads/a8e661b69e34058ee3e9f970730d5c9b/librewolf-${version}.dmg";
    sha256 = "sha256-wHyRO0a9yVOUBIfzSUrvoNCFZynlQ50KcDIHpk/Gm44=";
  };

  nativeBuildInputs = [ undmg ];

  sourceRoot = ".";

  installPhase = ''
    mkdir -p $out/Applications/
    cp -r *.app $out/Applications
  '';

  meta = with lib; {
    description =
      "A fork of Firefox, focused on privacy, security and freedom.";
    homepage = "https://librewolf.net/";
    license = licenses.mpl20;
    platforms = platforms.all;
    maintainers = with maintainers; [ self ];
  };
}
