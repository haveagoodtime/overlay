{ lib, stdenvNoCC, fetchzip }:

stdenvNoCC.mkDerivation rec {
  pname = "yabai";
  version = "4.0.0-1";

  src = fetchzip {
    url =
      "https://github.com/koekeishiya/yabai/files/7915231/yabai-v4.0.0.tar.gz";
    #url =
    # "https://github.com/azuwis/yabai/releases/download/v3.3.10/yabai-v${version}.tar.gz";
    sha256 = "sha256-qJE2zG7YIwYMJ9gWsguWIuFMPbvkD4lspRZKFiFkCYo";
  };

  installPhase = ''
    mkdir -p $out/bin
    mkdir -p $out/share/man/man1/
    cp ./bin/yabai $out/bin/yabai
    cp ./doc/yabai.1 $out/share/man/man1/yabai.1
  '';

  meta = with lib; {
    description = "yabai latest bin";
    homepage = "https://github.com/koekeishiya/yabai";
    license = licenses.mit;
    platforms = platforms.all;
    maintainers = with maintainers; [ self ];
  };
}
