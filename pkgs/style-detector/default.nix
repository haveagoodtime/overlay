{ lib, fetchzip }:
let version = "1.0";
in fetchzip {
  name = "style-detector";
  url =
    "https://gitlab.com/haveagoodtime/style-detector/uploads/c28eb1188b4f86526191efbc5b6b6124/style-detector.zip";
  sha256 = "sha256-TMhw+PkJpn2EkhNXeamJA8fPAb7vIgiy/SkiRHDkaSg=";
  postFetch = ''
    mkdir $out
    unzip $downloadedFile -d $out/
      '';
  meta = with lib; {
    description = ''
      Detect the current theme of macos and execute the command.
    '';
    homepage = "https://github.com/Lovely624/style-detector";
    platforms = platforms.all;
    maintainers = with maintainers; [ self ];
  };
}
